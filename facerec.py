
        #!/usr/bin/env python3

import os




import torch
from torch.distributions import MultivariateNormal

import cv2
from insightface.app import FaceAnalysis
from insightface.utils import face_align
import torch

import subprocess
import os

import random


class InitFunction:
    def __init__(self) -> None:
        pass



class FilesProcess:
    def __init__(self, image_path):
        self.image_path = image_path


    def process(self):
        face_files = []
        output = os.listdir(self.image_path)
        for outputs in output:
            face_image = os.listdir(self.image_path + '/' +outputs)
            for face in face_image:
                face_files.append(self.image_path + '/' +outputs + '/' + face)
                

                
        return(face_files)




class DataSamplingFace:
    def __init__(self, image_path):
        self.image_path = '/home/emmanuel/projects/lfw-deepfunneled/lfw-deepfunneled'
        self.face_embeddings = []
        


    def embeddings(self, pair_i):
        app = FaceAnalysis(name="buffalo_l", providers=['CUDAExecutionProvider', 'CPUExecutionProvider'])
        app.prepare(ctx_id=0, det_size=(640, 640))

        image = cv2.imread(pair_i)
        faces = app.get(image)
        faceid_embeds = torch.from_numpy(faces[0].normed_embedding).unsqueeze(0)
    
        return(faceid_embeds)
    


    def sampler(self):
        mean = torch.mean
        covariance_matrix = torch.cov
        distribution = MultivariateNormal(mean, covariance_matrix)




    def faces_embeddings(self):
        ProcessFile = FilesProcess(self.image_path)
        face_files = ProcessFile.process()
        sampling_indxes = range(len(face_files))

        random_samples = random.sample(sampling_indxes, 5)
        for faces in random_samples:
            self.face_embeddings.append(self.embeddings(face_files[faces]))



        return(self.face_embeddings)






    def sim(self, face_embeds, face_embedsx):
        similarity = torch.cosine_similarity(face_embeds, face_embedsx, dim=1)
        return(similarity)
    



    def face_bounderies(self):
        faces_pairs = self.faces_embeddings()
        for face in list(zip(faces_pairs[0::1], faces_pairs[0::2])):
            print(self.sim(face[0], face[1]))
    


    






'''''
    non_path = []
    path = []


    sim_samples = []
    sim_nills = []

    for nill in non_path:
        sim_samples.append(sim(nill[0], nill[1]))

    for nill in path:
        sim_nills.append(sim(nill[0], nill[1]))


    sim_nills.cov()
    sim_samples.cov()

    sim_nills.mean()
    sim_samples.mean()
'''




class ProcessImage:
    def __init__(self, image_path='/home/emmanuel/projects/lfw-deepfunneled/lfw-deepfunneled'):
        self.image_path = image_path


    def process(self):
        DataSampling = DataSampling(self.image_path)
        faces = DataSampling.face_pair(self.image_path)


def unit_test():
    SamplingFace = DataSamplingFace('/home/emmanuel/projects/lfw-deepfunneled/lfw-deepfunneled')
    faces = SamplingFace.face_bounderies()



    




unit_test()






